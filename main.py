from dataclasses import dataclass, asdict, field
import subprocess
import json
from sys import getsizeof
from os import scandir
from concurrent.futures import ThreadPoolExecutor

@dataclass(frozen=True)
class File:
    """
    """
    name: str
    path: str       # full path
    size: int       # add repr to do kb conversion - round(stat.st_size/(1024**2),3)
    owner: str      # uid/gid
    hash: str = ""
    def __repr__(self):
        return asdict(self)
    @staticmethod
    def generate_hash(f:str) -> str:
        fout = subprocess.run(["md5sum", f], capture_output=True)
        return fout.stdout.decode().rstrip().split(" ")[0]

@dataclass(frozen=True, repr=True)
class Directory:
    name: str
    path: str
    size: int
    subDirs: list = field(default_factory=list)
    files: list[File] = field(default_factory=list)

    def __repr__(self):
        return asdict(self)

class Dirpy:
    def __init__(self, root) -> None:
        """
        """
        self._root: Directory = self.scan_directory(root)
        self._next: list = self._root.subDirs
        self.data: dict[str, Directory] = {self._root.name:self._root}

    def __repr__(self):
        """ __repr__() - dumps str of indented JSON.

        Converts Directory() references in Dirpy.data to dict's.
        """  
        return json.dumps({n : asdict(d) for n, d in self.data.items()}, indent=4)

    def scan(self) -> None:
        """ scan() - Dirpy's main event loop. Scans until no sub-directories found.
        """
        while len(self._next) > 0:
            print(f"scanning next level...\n")
            _n = self._next
            with ThreadPoolExecutor() as pool:
                _done = pool.map(self.scan_directory, [_d for _d in _n])
            self._next.clear()
            for sd in _done:
                self.data[sd.name] = sd
                if len(sd.subDirs) > 0:
                    self._next + sd.subDirs
                

    def scan_directory(self, fp:str, file_hash:bool = True, file_stats: bool = False) -> Directory:
        #?: only needs self if updated say a total file count
            #?: @staticmethod it? dirpy.scan all dirpy.parse() a single Dir
        """
        """
        print(f"scanning {fp} ...\n")
        files = []
        files_size = []
        subDirs = []
        for item in scandir(fp):
            if item.is_file():
                fstat = item.stat()
                files_size.append(fstat.st_size)
                _fdata: list = [item.name, item.path, fstat.st_size, f"{fstat.st_uid}/{fstat.st_gid}"]
                if file_hash == True:
                    _fdata.append(
                        File.generate_hash(
                            item.path
                            )
                         )           
                files.append(
                        File(*_fdata)
                        )  
            elif item.is_dir():
                subDirs.append(item.path)
            else:
                continue
        return Directory(
            name=fp.split("/")[-1], path=fp, subDirs=subDirs,
            files=files, size=sum(files_size)
            )


if __name__ == "__main__":
    test = Dirpy("/home/unkwn1/Downloads")
    test.scan()
    print(test)
    print(f"test size: {getsizeof(test.data)}")