
#%%
import os
from dataclasses import dataclass, asdict, field
import subprocess
import json
from sys import getsizeof
from concurrent.futures import ThreadPoolExecutor
# %%

@dataclass(frozen=True)
class File:
    """ File() stores records of a scanned file in a Directory.

        generate_hash()
          - a static method that generates an md5 hash for a file.
          - File() is frozen hash must be supplied during init (see scan_dir() File call)

          Args:
            - f:str - path to a file to md5 hash
    """
    name: str
    path: str       # full path
    size: int       # add repr to do kb conversion - round(stat.st_size/(1024**2),3)
    owner: str      # uid/gid
    hash: str = ""
    @staticmethod
    def generate_hash(f:str) -> str:
        fout = subprocess.run(["coreutils", "md5sum", f], capture_output=True)
        return fout.stdout.decode().rstrip().split(" ")[0]
    
@dataclass(unsafe_hash=True, frozen=False)
class Directory:
    name: str
    path: str
    size: int               # sum of files + sub directory size
    subDirs: list = field(default_factory=list, hash=False)
    files: list[File] = field(default_factory=list, hash=False)
    def __post_init__(self):   
        if len(self.subDirs) > 0:
            with ThreadPoolExecutor() as pool:
                self.subDirs = [sd for sd in pool.map(Scan, [d for d in self.subDirs])]
            #self.subDirs = [Scan(d, level) for d in self.subDirs]
            #[size.append(d.size) for d in self.subDirs]
            #self.size = self.size + sum(d.size for d in self.subDirs)
            self.size = sum(d.size for d in self.subDirs)

    #def __repr__(self) -> str:
        #return json.dumps(asdict(self), indent=4)
    
  
def Scan(fp:str, level:int = 0, depth:int=0, file_hash:bool = True, file_stats: bool = False) -> Directory:
    """Takes a directory path and returns a nested  Directory object.
        Args:
        - fp:str - the absolute path to the source directory
        - depth:int - limits the depth of a scan
        - level:int - defaults to zero and is auto set. Leave it alone\n
        Return:
        - Directory() - nested, subdirs will be processed from strings to Directory()'s

        TODO: Break this up, subdir loop has to be here to catch level/depth param's
    """
    files = []
    files_size = []
    subDirs = []
    for item in os.scandir(fp):
        if item.is_file():
                fstat = item.stat()
                files_size.append(fstat.st_size)
                _fdata: list = [item.name, item.path, fstat.st_size, f"{fstat.st_uid}/{fstat.st_gid}"]
                if file_hash == True:
                    _fdata.append(
                        File.generate_hash(
                            item.path
                            )
                         )           
                files.append(
                        File(*_fdata)
                        ) 
        elif item.is_dir():
            subDirs.append(item.path)
        else:
            continue

        # TODO: check for depth / level and if subDirs not empty.
    return Directory(
        name=fp.split("/")[-1], path=fp, subDirs=subDirs,
        files=files, size=sum(files_size)
        )

# %%

test = Scan("/home/unkwn1/Downloads", file_hash=True)
#print(test)


#with open("output_test.json", "w") as f:
    #f.write(json.dumps(asdict(test), indent=4))

# %%
if __name__ == "__main__":
    test = Scan("/home/unkwn1/Downloads", file_hash=True)
    print(asdict(test))
    print(f"size of test: {getsizeof(test.subDirs)}")
